""" Solution of CliffWalking problem using Q-learning algorithm
with Upper Confidence Bound exploration algorithm.
"""

from collections import deque
import random

import numpy as np
from tqdm import tqdm

from common_components import env, n_states, n_actions, q2best_policy, play_episode

EPOCHS = 1000
STEPS = 100000  # max steps for one episode (per epoch)
MONITORING_STEP = 50

GAMMA = 0.95
ADD_FINISH_REWARD = 0  # optionally add reward to finish state

C = 10  # exploration coefficient in UCB
ALPHA = 1  # learning rate


def ucb(s: int  # state
        ) -> int:  # action to use
    """Returns UCB based action to use."""
    t = T[state].sum()
    if t <= 0:
        return random.randint(0, n_actions - 1)
    res = Q[state] + C * np.sqrt(np.log(t) / (T[state] + 1e-9))
    # if state == 36:
    #     print(state, np.argmax(res), np.argmax(Q[state]), res, Q[state])
    res = np.argmax(res)
    return res


Q = np.zeros((n_states, n_actions))  # values of each action on each state (initialized with zeros)
T = np.zeros((n_states, n_actions), dtype=int)  # counter of times an actions was used
pbar = tqdm(range(EPOCHS), ncols=100)
for epoch in pbar:
    ep_lens = deque(maxlen=100)
    state = env.reset()
    for step in range(STEPS):
        action = ucb(state)
        new_state, reward, is_done, info = env.step(action)

        # add extra reward to target (finish) state inasmach as we want to reach this state
        if new_state == 47:
            reward += ADD_FINISH_REWARD

        # update Q
        # Q[state, action] += ALPHA * (reward + GAMMA * Q[new_state, np.argmax(Q[new_state])] - Q[state, action])
        Q[state, action] += ALPHA * (reward + GAMMA * max(Q[new_state]) - Q[state, action])
        T[state, action] += 1

        if is_done:
            break

        state = new_state

    ep_lens.append(step + 1)

    # monitoring
    if not epoch % MONITORING_STEP:
        # print(Q)
        # print(np.arange(0, 48).reshape(4, 12))
        P = q2best_policy(Q)
        print('\n', repr(np.array(P).reshape(4, -1)).translate(str.maketrans('0123', '↑→↓←'))
              .replace('array(', '').replace(' ' * 6, ' '))

    pbar.set_postfix({'alpha': ALPHA,
                      'C': C,
                      'summax': np.max(Q, axis=1).sum(),
                      'mean_ep_len': sum(ep_lens) / len(ep_lens)})


P = q2best_policy(Q)

print(Q)

print(np.arange(0, 48).reshape(4, 12))
print('\n', repr(np.array(P).reshape(4, -1)).translate(str.maketrans('0123', '↑→↓←'))
      .replace('array(', '').replace(' ' * 6, ' '))

play_episode(Q, epsilon=0.1, render=True, slp=0.7)
