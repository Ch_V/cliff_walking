import random
from time import sleep

import gym
import numpy as np


EPISODES_N = 1000

env = gym.make('CliffWalking-v0')

n_states = len(env.env.P)
n_actions = len(env.env.P[0])
i2a = {0: 'up', 1: 'right', 2: 'down', 3: 'left'}  # integer-to-action


def get_random_policy() -> np.array:
    """Generates and returns random policy - numpy array of length n_states."""
    return np.random.randint(0, n_actions, (n_states,))


def q2best_policy(Q: np.array) -> np.array:
    """Returns best policy based on input Q-function values."""
    P = np.zeros(n_states).astype('int8')  # policy (strategy)
    for state in range(n_states):
        P[state] = Q[state].argmax()
    return P


def play_episode(Q: np.array, epsilon: float = 0, render: bool = False, slp: float = 0.5):
    """ Plays episode once with policy policy.
    render - should the play be rendered with slp sleep-time between steps.
    Returns total reward and list of states.
    """
    state = env.reset()
    total_reward = 0
    states = []
    while True:
        if render:
            env.render()
            sleep(slp)
        states.append(state)
        action = np.random.choice([np.argmax(Q[state]).item(), random.choice(range(n_actions))], p=(1 - epsilon, epsilon))
        state, reward, is_done, info = env.step(action)
        total_reward += reward
        if is_done:
            break
    return total_reward, states


if __name__ == '__main__':
    Q = np.random.random((n_states, n_actions))
    P = q2best_policy(Q)
    print(np.array(P).reshape(4, -1))
    # play_episode(Q=np.random.random((n_states, n_actions)), epsilon=0.2, render=True, slp=0.5)