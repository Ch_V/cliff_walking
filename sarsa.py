"""Solution of CliffWalking problem using SARSA algorithm."""

from collections import deque
import random

import numpy as np
from tqdm import tqdm

from common_components import env, n_states, n_actions, q2best_policy, play_episode


EPOCHS = 10000
STEPS = 1000000  # max steps for one episode (per epoch)
MONITORING_STEP = 50

GAMMA = 0.95
ADD_FINISH_REWARD = 100  # optionally add reward to finish state

EPSILON = 0.3
ALPHA = 0.1  # learning rate

Q = np.zeros((n_states, n_actions))  # values of each action on each state (initialized with zeros)
pbar = tqdm(range(EPOCHS), ncols=100)
for epoch in pbar:
    ep_lens = deque(maxlen=100)
    state = env.reset()
    action = np.random.choice([np.argmax(Q[state]).item(), random.choice(range(n_actions))], p=(1 - EPSILON, EPSILON))
    for step in range(STEPS):
        new_state, reward, is_done, info = env.step(action)
        new_action = np.random.choice([np.argmax(Q[state]).item(), random.choice(range(n_actions))], p=(1 - EPSILON, EPSILON))

        # add extra reward to target (finish) state inasmach as we want to reach this state
        if new_state == 47:
            reward += ADD_FINISH_REWARD

        # update Q
        Q[state, action] += ALPHA * (reward + GAMMA * Q[new_state, new_action] - Q[state, action])

        if is_done:
            break

        state = new_state
        action = new_action

    ep_lens.append(step + 1)

    # monitoring
    if not epoch % MONITORING_STEP:
        # print(Q)
        # print(np.arange(0, 48).reshape(4, 12))
        P = q2best_policy(Q)
        print('\n', repr(np.array(P).reshape(4, -1)).translate(str.maketrans('0123', '↑→↓←'))
              .replace('array(', '').replace(' ' * 6, ' '))

    pbar.set_postfix({'alpha': ALPHA,
                      'epsilon': EPSILON,
                      'summax': np.max(Q, axis=1).sum(),
                      'mean_ep_len': sum(ep_lens) / len(ep_lens)})


P = q2best_policy(Q)

print(Q)

print(np.arange(0, 48).reshape(4, 12))
print('\n', repr(np.array(P).reshape(4, -1)).translate(str.maketrans('0123', '↑→↓←'))
      .replace('array(', '').replace(' ' * 6, ' '))

play_episode(Q, epsilon=0.1, render=True, slp=0.7)
