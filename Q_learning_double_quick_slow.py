"""Solution of CliffWalking problem using Double Q-learning algorithm with one quick and one slow updatable Q."""

from collections import deque
import random

import numpy as np
from tqdm import tqdm

from common_components import env, n_states, n_actions, q2best_policy, play_episode

EPOCHS = 3000
STEPS = 1000000  # max steps for one episode (per epoch)
UPDATE_INTERVAL = 5  # update Qs every UPDATE_INTERVAL steps
MONITORING_STEP = 50  # 500

GAMMA = 0.95
ADD_FINISH_REWARD = 0  # optionally add reward to finish state

EPSILON = 0.3  # for epsilon-greedy behaviour policy
ALPHA = 1  # learning rate

Qf = np.zeros((n_states, n_actions))  # fast one
Qs = np.zeros((n_states, n_actions))  # slow one
pbar = tqdm(range(EPOCHS), ncols=100)
for epoch in pbar:
    ep_lens = deque(maxlen=100)
    state = env.reset()
    for step in range(STEPS):
        action = np.random.choice([np.argmax(Qf[state]).item(), random.choice(range(n_actions))], p=(1 - EPSILON, EPSILON))
        new_state, reward, is_done, info = env.step(action)

        # add extra reward to target (finish) state inasmach as we want to reach this state
        if new_state == 47:
            reward += ADD_FINISH_REWARD

        # update Q
        # Qf[state, action] += ALPHA * (reward + GAMMA * Qs[new_state, np.argmax(Qs[new_state])] - Qf[state, action])
        Qf[state, action] += ALPHA * (reward + GAMMA * max(Qs[new_state]) - Qf[state, action])

        if is_done:
            break

        state = new_state

        # if not (step + 1) % UPDATE_INTERVAL:
        #     Qs = Qf.copy()
    if not (epoch + 1) % UPDATE_INTERVAL:
        Qs = Qf.copy()

    ep_lens.append(step + 1)

    # monitoring
    if not epoch % MONITORING_STEP:
        # print(Q)
        # print(np.arange(0, 48).reshape(4, 12))
        P = q2best_policy(Qs)
        print('\n', repr(np.array(P).reshape(4, -1)).translate(str.maketrans('0123', '↑→↓←'))
              .replace('array(', '').replace(' ' * 6, ' '))

    pbar.set_postfix({'alpha': ALPHA,
                      'epsilon': EPSILON,
                      'summax': np.max(Qs, axis=1).sum(),
                      'mean_ep_len': sum(ep_lens) / len(ep_lens)})

Q = Qf.copy()
P = q2best_policy(Q)

print(Q)

print(np.arange(0, 48).reshape(4, 12))
print('\n', repr(np.array(P).reshape(4, -1)).translate(str.maketrans('0123', '↑→↓←'))
      .replace('array(', '').replace(' ' * 6, ' '))

play_episode(Q, epsilon=0.1, render=True, slp=0.7)
