"""Here toy can play CliffWalking game manual by entering step-corresponding digits into input."""

from common_components import env

state = env.reset()

total_reward = 0
is_done=False
while not is_done:
    env.render()
    try:
        action = int(input("0 <-> 'up'\n"
                           "1 <-> 'right'\n"
                           "2 <-> 'down'\n"
                           "3 <-> 'left'\nEnter your step (enter digit): "))
        if action not in range(4):
            raise Exception
    except Exception:
        print("\nWRONG STEP!!! You should enter one if this digits: 0, 1, 2, 3.")
        continue

    new_state, reward, is_done, info = env.step(action)
    total_reward += reward
    print('total_reward =', total_reward, ', step reward =', reward)
    print('-' * 100)

env.render()