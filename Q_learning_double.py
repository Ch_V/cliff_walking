"""Solution of CliffWalking problem using Double Q-learning algorithm."""

from collections import deque
import random

import numpy as np
from tqdm import tqdm

from common_components import env, n_states, n_actions, q2best_policy, play_episode

EPOCHS = 3000
STEPS = 1000000  # max steps for one episode (per epoch)
MONITORING_STEP = 50  # 500

GAMMA = 0.95
ADD_FINISH_REWARD = 0  # optionally add reward to finish state

EPSILON = 0.3  # for epsilon-greedy behaviour policy
ALPHA = 1  # learning rate

Q1 = np.zeros((n_states, n_actions))
Q2 = np.zeros((n_states, n_actions))
pbar = tqdm(range(EPOCHS), ncols=100)
for epoch in pbar:
    ep_lens = deque(maxlen=100)
    state = env.reset()
    for step in range(STEPS):
        Qn = random.choice([1, 2])  # which of Qn use on this step for exploit and which for explore

        action = np.random.choice([np.argmax((Q1, Q2)[Qn - 1][state]).item(), random.choice(range(n_actions))],
                                  p=(1 - EPSILON, EPSILON))
        new_state, reward, is_done, info = env.step(action)

        # add extra reward to target (finish) state inasmach as we want to reach this state
        if new_state == 47:
            reward += ADD_FINISH_REWARD

        # update Q
        (Q1, Q2)[Qn - 1][state, action] += ALPHA * (
                reward +
                GAMMA * (Q2, Q1)[Qn - 1][new_state, np.argmax((Q1, Q2)[Qn - 1][new_state])] -
                (Q1, Q2)[Qn - 1][state, action]
        )

        if is_done:
            break

        state = new_state

    ep_lens.append(step + 1)

    # monitoring
    Q = np.mean([Q1, Q2], axis=0)
    if not epoch % MONITORING_STEP:
        # print(Q)
        # print(np.arange(0, 48).reshape(4, 12))
        P = q2best_policy(Q)
        print('\n', repr(np.array(P).reshape(4, -1)).translate(str.maketrans('0123', '↑→↓←'))
              .replace('array(', '').replace(' ' * 6, ' '))

    pbar.set_postfix({'alpha': ALPHA,
                      'epsilon': EPSILON,
                      'summax': np.max(Q, axis=1).sum(),
                      'mean_ep_len': sum(ep_lens) / len(ep_lens)})


P = q2best_policy(Q)

print(Q)

print(np.arange(0, 48).reshape(4, 12))
print('\n', repr(np.array(P).reshape(4, -1)).translate(str.maketrans('0123', '↑→↓←'))
      .replace('array(', '').replace(' ' * 6, ' '))

play_episode(Q, epsilon=0.1, render=True, slp=0.7)
